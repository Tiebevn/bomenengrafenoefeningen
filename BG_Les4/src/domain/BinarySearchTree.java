package domain;

public class BinarySearchTree<E extends Comparable<E>> {
		private E data;
		private BinarySearchTree<E> leftTree, rightTree;
		
		public BinarySearchTree(){
		}
		
		
		
		private  BinarySearchTree(E data){
			this.data = data;
		}
		
		
		
		public boolean lookup(E data){
			if ( data == null || this.data == null) return false;
			else {
				if (data.compareTo(this.data) == 0) return true;
				else if (data.compareTo(this.data) <  0){
					return (this.leftTree == null?false:leftTree.lookup(data));
				}
				else {
					return (this.rightTree == null? false: rightTree.lookup(data));
				}
			}
		}
		
	public void printPreorder(){
		System.out.print(this.data + " ");
		if (this.leftTree != null) this.leftTree.printPreorder();
		if (this.rightTree != null) this.rightTree.printPreorder();
	}
	
	public void printInOrder() {
		if (this.leftTree != null) this.leftTree.printInOrder();
		System.out.print(this.data + " ");
		if (this.rightTree != null) this.rightTree.printInOrder();
		
	}
	
	public void printPostOrder() {
		
		if (this.leftTree != null) this.leftTree.printPostOrder();
		if (this.rightTree != null) this.rightTree.printPostOrder();
		System.out.print(this.data + " ");
		
	}
		
		public boolean addNode(E data){
			
			if (data == null) return false;
			if (this.data == null) {
				this.data = data;
				return true;
			}
			
			if (data.compareTo(this.data) == 0){
				
				return false;
			} else if (data.compareTo(this.data) < 0) {
				
				if (this.leftTree == null) {
					this.leftTree = new BinarySearchTree<E>(data);
					return true;
				} else return this.leftTree.addNode(data);
				
			} else {
				
				if (this.rightTree == null) {
					this.rightTree = new BinarySearchTree<E>(data);
					return true;
				}else return this.rightTree.addNode(data);
				
			}
			
		}
		
		public boolean removeNode(E data){
			if (data == null || this.data == null) return false;
			
			
			if (data.compareTo(this.data) == 0) {
				this.data = null;
				return true;
			} else if (data.compareTo(this.data) < 0) {
				if (this.leftTree == null) {
					return false;
				} else return this.leftTree.removeNode(data);
			} else {
				if (this.rightTree == null) {
					return false;
				} else return this.rightTree.removeNode(data);
			}
		}

		public E searchGreatest() {
			if (this.rightTree == null) {
				return this.data;
			} else return this.rightTree.searchGreatest();
		}


		public E searchSmallest() {
			if (this.leftTree == null) {
				return this.data;
			} else return this.leftTree.searchSmallest();
		}
		
		
		public  void ruimOp() {
			
			BinarySearchTree<E>result = new BinarySearchTree<E>();
			
			
			
			
			
		}
		
		
}
