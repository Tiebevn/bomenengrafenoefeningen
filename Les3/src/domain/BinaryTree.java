package domain;

import java.util.ArrayList;

public class BinaryTree<E> {
	private E data;
	private BinaryTree<E> leftTree, rightTree;
	
	public BinaryTree(E data){
		this(data,null,null);
	}
	
	public BinaryTree(E data, BinaryTree<E> leftTree, BinaryTree<E> rightTree){
		this.data= data;
		this.leftTree= leftTree;
		this.rightTree= rightTree;
	}
	
	public void printPreorder(){
			System.out.print(this.data + " ");
			if (this.leftTree != null) this.leftTree.printPreorder();
			if (this.rightTree != null) this.rightTree.printPreorder();
	}
	
	public void printInOrder() {
		if (this.leftTree != null) this.leftTree.printInOrder();
		System.out.print(this.data + " ");
		if (this.rightTree != null) this.rightTree.printInOrder();
		
	}
	
	public void printPostOrder() {
		
		if (this.leftTree != null) this.leftTree.printPostOrder();
		if (this.rightTree != null) this.rightTree.printPostOrder();
		System.out.print(this.data + " ");
		
	}
	
	public int countKnots() {
	
		
		int n = 1;
		if (null != this.leftTree) n += this.leftTree.countKnots();
		if (null != this.rightTree) n += this.rightTree.countKnots();
		return n;
		
	}
	
	public int maxDepth() {
		
		int n = 0;
		
		
		if (this.leftTree != null) n = Math.max(n, this.leftTree.maxDepth());
		if (this.rightTree != null) n = Math.max(n, this.rightTree.maxDepth());
		
		return n + 1;
	}
	
	public boolean isLeaf(){
		return (null == this.leftTree && null == this.rightTree);
	}
	
	public int countLeaves() {
		int n = 0;
		if (this.isLeaf()) n = 1;
		if (this.leftTree != null) n += this.leftTree.countLeaves();
		if (this.rightTree != null) n +=  this.rightTree.countLeaves();
		return n;
	}
	
	public ArrayList<E> getDataLeaves() {
		ArrayList<E> output = new ArrayList<>();
		if (this.isLeaf()) output.add(this.data);
		if (this.leftTree != null) output.addAll(this.leftTree.getDataLeaves());
		if (this.rightTree != null) output.addAll(this.rightTree.getDataLeaves());
		return output;
	}
	
	public boolean contains(E value) {
		
		boolean lbool = false;
		boolean rbool = false;
		
		if (this.data.equals(value)) return true;
		if (this.leftTree != null) lbool = this.leftTree.contains(value);
		if (this.rightTree != null) rbool = this.rightTree.contains(value);
		
		return lbool || rbool;
		
	}

}
