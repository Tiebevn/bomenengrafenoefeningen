import java.util.ArrayList;
import java.util.List;

public class Recursie {

	
	// Oefening 1: n-de Fibonacci-getal
	
	public static int fibonacci(int getal){
		
		if (getal <= 0) throw new IllegalArgumentException();
		
		
		if (getal == 1 || getal == 2) return 1;
		
		return fibonacci(getal - 1) + fibonacci(getal - 2);	
		
	}
	
	// Oefening 2 : som van cijfers
	
	public static int somVanCijfers(int getal){
		
		getal = Math.abs(getal);
		
		if (getal < 10) return getal;
		
		
		return (getal % 10) + somVanCijfers(getal / 10);
		
	}
	
	// Oefening 3: keer een string om
	
	public static String keerOm(String s){
	
		if (null == s) throw new IllegalArgumentException();
		
		if (s.length() == 0) 
	         return s;

	    return keerOm(s.substring(1)) + s.charAt(0);
	}
	
	//oefening 4: countX
	
	public static int countX(String s){
		
		if (null == s) throw new IllegalArgumentException();
		if (s.length() == 0) return 0;
		
		int counter = 0;
		
		if (s.charAt(0) == 'x') counter = 1;
		
		return counter + countX(s.substring(1));
		
		
	}
	
	//oefening 5 : countHi
	public static int countHi(String s){
		
		
		if (null == s) throw new IllegalArgumentException();
		
		if (s.length() < 2) return 0;
		
		
		if (s.charAt(0) == 'h' && s.charAt(1) == 'i')  {
			return 1 + countHi(s.substring(2));
		} else return 0 + countHi(s.substring(1));
		
	}
	
	// oefening 6
	public static String changeXY(String s){

		if (null == s) throw new IllegalArgumentException();
		
		if (s.length() <= 0) return "";
		
		if (s.charAt(0) == 'x') {
			return "y" + changeXY(s.substring(1));
		} else return s.substring(0, 1) + changeXY(s.substring(1));
		
	}
	
	// oefening 7
	
	public static String changePi(String s){
		
		if (null == s) throw new IllegalArgumentException();
		
		if (s.length() < 2) return s;
		
		if (s.charAt(0) == 'p' && s.charAt(1) == 'i') {
			return "3.14" + changePi(s.substring(2));
		} else return s.substring(0, 1) + changePi(s.substring(1));
		
	}
	
	// oefening 8:
	public static int tweelog(int getal){
		
		if (getal <= 0) throw new IllegalArgumentException();
		
		if (getal == 1) return 0;
		
		if (getal == 2) return 1;

		return 1 + tweelog(getal/2);
		
		
		
	}
	
	// oefening 9;
	public static double findMaximum(List<Double> lijst){
		
		if (null == lijst || lijst.isEmpty()) throw new IllegalArgumentException();
		
		
		if (lijst.size() == 1) return lijst.get(0);
		
		if (lijst.get(0) >= lijst.get(1)) {
			lijst.remove(1);
			return findMaximum(lijst);
		} else {
			lijst.remove(0);
			return findMaximum(lijst);
		}
	}
	
	// oefening 10;
	public static ArrayList<String> findSubstrings(String string){
		ArrayList<String> list = new ArrayList<>();
		
		if (null == string) throw new IllegalArgumentException();
		
		if (string.length() == 1) {
			list.add(string);
			return list;
		}
		
		
		if (list.contains(string)) {
			
		} else list.add(string);
		
		
		for (int i = 0; i < string.length(); i++) {
			
			ArrayList<String> underList = findSubstrings(removeCharFromString(string, i));
			
			for (String item : underList) {
				
				if (!list.contains(item)) list.add(item);
				
			}
			 
			 
			 System.out.println(list);
		}
		
		
		
		
		return list;
	}
	
	public static String removeCharFromString(String string, int index) {
		
		if (1 == string.length()) return string;
		
		if (index == string.length() - 1) return string.substring(0, string.length() - 1);
		
		if (index == 0) return string.substring(1);
		
		string = string.substring(0, index) + string.substring(index + 1);
		
		return string;
	}
	
}